FROM ubuntu:22.04

LABEL MAINTAINER="lqb@gmx.de"

COPY entrypoint.sh /
COPY samba-ad-backup /usr/local/bin

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive apt-get install -y \
      vim \
      bash \
      samba \
      krb5-config \
      krb5-user \
      winbind \
      smbclient \
      bind9-dnsutils \
      net-tools \
      gettext-base \
      iputils-ping \
      iproute2 \
 && rm -rf /var/lib/apt/lists/*

VOLUME /etc/samba /var/lib/samba

ENV REALM=samdom.example.com \
    WORKGROUP=samdom \
    ACTION=run

ENTRYPOINT ["/entrypoint.sh"]

EXPOSE 53/udp 88/udp 137/udp 138/udp 389/udp 464/udp 53/tcp 88/tcp 135/tcp 139/tcp 389/tcp 445/tcp 464/tcp 636/tcp 3268/tcp 3269/tcp 49152/tcp 49153/tcp 49154/tcp

